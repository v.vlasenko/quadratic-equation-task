package com.epam.rd.autotasks;

import java.util.Locale;
import java.util.Scanner;

public class QuadraticEquation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();
        var discriminant = (b*b) - (4*a*c);
        var rootWithPlusSign = (-b + Math.sqrt(discriminant)) / (2 * a);
        var rootWithMinusSign = (-b - Math.sqrt(discriminant))/(2 * a);

        if (discriminant > 0) {
            System.out.println(rootWithPlusSign + " " + rootWithMinusSign);
        } else if (discriminant==0) {
            System.out.println(rootWithPlusSign);
        } else {
            System.out.println("no roots");
        }
    }
}